﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="EX_ConnectDB_MSSQL.Index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder" runat="server">
    <div class="row col-md-12">
        <div class="col-md-8">
            <asp:TextBox ID="txtSearch" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="col-md-4">
            <asp:Button ID="Button1" CssClass="btn btn-success" runat="server" Text="Button" OnClick="Button1_Click" />
        </div>        
    </div>
    <div class="row col-md-12">
            <div class="col-md-12">                
                <asp:GridView ID="GridView1" CssClass="table table-condensed table-hover table-striped" runat="server"></asp:GridView>
            </div>            
        </div>
</asp:Content>
