﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace EX_ConnectDB_MSSQL
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bindingdata("");
            }
        }

        private void Bindingdata(string para)
        {
            try
            {
                string Connection = @"Data Source=IMAGINE-PC; Initial Catalog=CUSTOMER_DB;User id=sa;Password=123456";
                //string Connection = @"Data Source=IMAGINE-PC; Initial Catalog=CUSTOMER_DB;User id=sa;Password=123456";
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = Connection;
                SqlCommand cmd = new SqlCommand("GET_CUSTOMER", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("Country", para));
                conn.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(dr);
                GridView1.DataSource = dt;
                GridView1.DataBind();

                conn.Close();
            }
            catch (Exception)
            {
                throw;
            }            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Bindingdata(txtSearch.Text);
        }
    }
}